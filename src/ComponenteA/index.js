import React from "react";

import Icon from './Icon';

import Body from './Body';
import styled from 'styled-components';

const Container=styled.div`  
height:100px
  width:350px;  
  display:flex;
  flex-direction:row;   
  margin-top:15px;
`;

const App=(params)=>{
    return (<Container>   
     <Icon ico={params.icon} coloricon={params.coloricon} ></Icon>    
     <Body></Body>    
      </Container>);
}

export default App;
