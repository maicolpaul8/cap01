//import logo from './logo.svg';
//import './App.css';
import styled from 'styled-components';


const Container=styled.div`  
  width:25%;      
`
const Body=styled.div`  
    background-color:${props=>props.coloricon};  
    height:100%;
    width:100%;
    border-radius:25px;   
    display:flex;
    align-items:center; 
`

const Icon=styled.img
` height:45px
`

const App=({ico,coloricon})=>{
    return (<Container>    
        <Body coloricon={coloricon}>
        <Icon  src={ico}></Icon>
        </Body>            
      </Container>);
}

export default App;
