import React from "react";



import styled from 'styled-components';

const Container=styled.div`  
    width:75%;
    padding-left:25px;
    display:flex;
    flex-direction:column;
    align-items:center;
`;

const Title=styled.p`
    margin-top:0px;
    color:#858ea3;
    font-weight:bold;
    font-size:15px;
`;

const Description=styled.p`
    margin:0px;
    margin-top:5px;
    color:#bcc1c5;
    font-size:14px;
`;

const Enlace=styled.a`
    margin:0px;
    margin-top:5px;
    color:#3b8699;
    font-size:14px;
`;

const App=()=>{
    return (<Container>    
        <Title>
            Paquete Premiun
            </Title>     
            <Description>
                Descubre nuevas funciones
            </Description>   
            <Enlace>
                Hazte Premiun
            </Enlace>
      </Container>);
}

export default App;
